<?php
require_once "Strategie.php";


final class Strategie_Agressive implements Strategie {
	public static function move($state, $idPlayer) {
		$direction = "Stay";
		
		//Initialisation
		$coordsJoueur = array($state["hero"]["pos"]["x"], $state["hero"]["pos"]["y"]);
		
		$map = new Map($state['game']['board'], $idPlayer + 1);
		$tableauDistance = $map->calculerDistanceEnnemis();
		$coordsTav = $map->obtenirTaverne($tableauDistance);
		
		$heroState = $state["game"]["heroes"][$idPlayer];
		$taille = $state["game"]["board"]["size"];
		
		$coordEnnemiProche = $map->obtenirEnnemiProche($tableauDistance);
		//Calcul de la direction à prendre
		if (($heroState["life"] < 30) && $tableauDistance[$coordEnnemiProche[0]][$coordEnnemiProche[1]] > 1 || (($tableauDistance[$coordsTav[0]][$coordsTav[1]] == 1) && ($heroState['gold'] > 0) && ($heroState["life"] <= 90))) {
			//Boire un coup
			$coords = $coordsTav;
			$tableauDistance = $map->reinitialiserEnnemi($tableauDistance);
		}
		else {
			//Attaquer
			$coords = $map->obtenirEnnemi($state["game"]["heroes"]);
			$tableauDistance = $map->reinitialiserTaverne($tableauDistance);
		}
		
		//On fait le chemin inverse
		if ($coords != array(-1, -1)) {
			while ($coords != $coordsJoueur) {
				$ligne = $coords[0];
				$colonne = $coords[1];
		
				//Aller à l'ouest
				if ($coords[1] < $taille - 1 && $tableauDistance[$ligne][$colonne + 1] == $tableauDistance[$ligne][$colonne] - 1)
				{
					$coords[1]++;
					$direction = 'West';
				}
		
				//Aller à l'est
				else if ($coords[1] > 0 && $tableauDistance[$ligne][$coords[1] - 1] == $tableauDistance[$ligne][$coords[1]] - 1)
				{
					$coords[1]--;
					$direction = 'East';
				}
		
				//Aller au nord
				else if ($coords[0] < $taille - 1 && $tableauDistance[$ligne + 1][$colonne] == $tableauDistance[$ligne][$colonne] - 1)
				{
					$coords[0]++;
					$direction = 'North';
				}
		
				//Aller au sud
				else if ($coords[0] > 0 && $tableauDistance[$ligne - 1][$colonne] == $tableauDistance[$ligne][$colonne] - 1)
				{
					$coords[0]--;
					$direction = 'South';
				}
			}
		}
		
		return $direction;
	}
	
	// else if($coords != array(-1,-1) && $tableauDistance[$coords[0]][$coords[1]] == 1) {
	// //Aller vers l'ennemi
	// echo "1";
	// if($coords[0] < $state["game"]["board"]["size"]-1 && $tableauDistance[$coords[0]+1][$coords[1]] == $tableauDistance[$coords[0]][$coords[1]]-1)
	// {
	// return 'North';
		// }
		// else if($coords[0] > 0 && $tableauDistance[$coords[0]-1][$coords[1]] == $tableauDistance[$coords[0]][$coords[1]]-1)
		// {
		// return 'South';
			// }
			// else if($coords[1] < $state["game"]["board"]["size"]-1 && $tableauDistance[$coords[0]][$coords[1]+1] == $tableauDistance[$coords[0]][$coords[1]]-1)
			// {
			// return 'West';
				// }
				// else if($coords[1] > 0 && $tableauDistance[$coords[0]][$coords[1]-1] == $tableauDistance[$coords[0]][$coords[1]]-1)
				// {
				// return 'East';
				// }
				// }
				// else if($coords != array(-1,-1) && $tableauDistance[$coords[0]][$coords[1]] == 2) {
				// //Attendre si il s'approche (de façon a être le premier a taper)
				// return '2';
				// }
}