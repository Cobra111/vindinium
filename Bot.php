<?php
require_once "Map.php";


class Bot
{
	/**
	 * Nom du joueur
	 * @var string
	 */
	protected $playerName;
	
	/**
	 * Map
	 * @var Map
	 */
	protected $map;
	
	
	/**
	 * Constructeur
	 * @param String $playerName
	 */
	public function __construct($playerName) {
		$this->playerName = $playerName;
	}
	
	
	/**
	 * Retourne l'indice du joueur dans le tableau heroes.
	 * @param array $state
	 * @return number
	 */
	protected function getNumPlayerInGame($state) {
		for ($i = 0; $i < 4; $i++) {
			if (isset($state["game"]["heroes"][$i]["name"]) && $state["game"]["heroes"][$i]["name"] == $this->playerName) {
				return $i;
			}
		}
		
		return -1;
	}
	
	
	/**
	 * Détermine si on choisit la stratégie agressive ou non.
	 * @param array $tabHeros
	 * @return boolean
	 */
	private function choisirStrategieAgressive($tabHeros) {		
		$nbMines = 0;
		$nbMinesAdv = 0;
		
		for ($i = 0; $i < 4; $i++) {
			if ($tabHeros[$i]["name"] != $this->playerName) {
				if (($tabHeros[$i]["mineCount"] > $nbMinesAdv)) {
					$nbMinesAdv = $tabHeros[$i]["mineCount"];
				}
			}
			else {
				if (($tabHeros[$i]["mineCount"] > $nbMines)) {
					$nbMines = $tabHeros[$i]["mineCount"];
				}
			}
		}
	
		if ($nbMinesAdv > $nbMines) {
			return true;
		}
		
		return false;
	}
	
	
	/**
	 * Retourne le mouvement choisi par l'IA pour le joueur
	 * @param array $state
	 * @return 'Stay', 'North', 'South', 'East', 'West'
	 */
	public function move($state) {		
		/*if ($this->choisirStrategieAgressive($state["game"]["heroes"])) {
			require_once "Strategie_Agressive.php";
 			return Strategie_Agressive::move($state, $this->getNumPlayerInGame($state));
		}
		else {*/
			require_once "Strategie_Mineur.php";
			return Strategie_Mineur::move($state, $this->getNumPlayerInGame($state));
		//}
	}
}