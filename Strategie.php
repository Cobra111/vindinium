<?php
interface Strategie {
	
	/**
	 * Retourne le mouvement choisi par l'IA pour le joueur
	 * @param array $state
	 * @return 'Stay', 'North', 'South', 'East', 'West'
	 */
	public static function move($state, $idPlayer);
}