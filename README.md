# Php starter pack for [vindinium](http://vindinium.org)

Inspired by [vindinium-starter-python](https://github.com/ornicar/vindinium-starter-python).

Should work with php5.3.* with CLI.

## Libraries deps:

    php5-cli
    cURL
    php5-curl

## Run with:

    php starter.php <key> <[training|arena]> <number-of-games-to-play> [server-url]

## Examples:

	php starter.php pqgs0wv0 training 30
    php starter.php pqgs0wv0 arena 1
    
