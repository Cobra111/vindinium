<?php
class Map {
	const VIDE = 0;
	const MUR = 1;
	const TAVERNE = 2;
	const MINE_NEUTRE = 3;
	const MA_POSITION = 4;
	const JOUEUR_ENNEMI = 5;
	
	/**
	 * Contenu de chaque morceau de la map.
	 * @var array
	 */
	protected $cases;
	
	/**
	 * Taille de la map
	 * @var int
	 */
	protected $taille;
	
	/**
	 * ID du joueur
	 * @var int
	 */
	protected $idJoueur;
	
	
	/**
	 * Nombre de mines dans la map
	 * @var int
	 */
	protected $nbMinesMap;
	
	
	public function __construct($tableau, $idJoueur) {
		$this->idJoueur = $idJoueur;
		$this->nbMinesMap = 0;
		$this->taille = $tableau["size"];
		$this->cases = array();
		$this->parserMap($tableau["tiles"]);
	}
	
	
	/**
	 * Retourne le nombre de mines de la map.
	 * @return number
	 */
	public function getNbMines() {
		return $this->nbMinesMap;
	}
	
	
	/**
	 * Parse la chaîne de caractères contenant la structure de la map, 
	 * et le stocke dans le tableau cases.
	 * @param string $structureMap
	 */
	private function parserMap($structureMap) {	
		for ($ligne = 0; $ligne < $this->taille; $ligne++) {
			$tabElements = array();
			
			for ($colonne = 0; $colonne < $this->taille; $colonne++) {
				$element = substr($structureMap, 2 * (($ligne * $this->taille) + $colonne), 2);
				
				switch ($element) {
					case "##" :
						array_push($tabElements, self::MUR);
						break;
					
					case "[]" :
						array_push($tabElements, self::TAVERNE);
						break;
					
					case "$-" :
						array_push($tabElements, self::MINE_NEUTRE);
						$this->nbMinesMap++;
						break;
						
					case "$" . $this->idJoueur :
						array_push($tabElements, self::MUR);
						$this->nbMinesMap++;
						break;
						
					case "@" . $this->idJoueur :
						array_push($tabElements, self::MA_POSITION);
						break;
						
					default :
						if ($element[0] === "$") {
							array_push($tabElements, self::MINE_NEUTRE);
							$this->nbMinesMap++;
						}
						else if ($element[0] === "@") {
							array_push($tabElements, self::JOUEUR_ENNEMI);
						}
						else {
							array_push($tabElements, self::VIDE);
						}
				}
			}
			
			array_push($this->cases, $tabElements);
		}
	}
	
	
	/**
	 * Renvoie les tableau de passage et de distance initialisés.
	 * @param array $tabEltsIgnores
	 * @return array:array
	 */
	private function initialiserCalculDistance($tabEltsIgnores) {
		//Phase d'initialisation
		$tabDistances = array();
		$tabPassages = array();
		
		
		for ($ligne = 0; $ligne < $this->taille; $ligne++) {
			$tabLigneDistances = array();
			$tabLignePassages = array();
		
			for ($colonne = 0; $colonne < $this->taille; $colonne++) {
				if (in_array($this->cases[$ligne][$colonne], $tabEltsIgnores)) {
					array_push($tabLigneDistances, -1);
					array_push($tabLignePassages, 1);
				}
				else if ($this->cases[$ligne][$colonne] == self::MA_POSITION) {
					array_push($tabLigneDistances, 0);
					array_push($tabLignePassages, 0);
				}
				else {
					array_push($tabLigneDistances, PHP_INT_MAX);
					array_push($tabLignePassages, 0);
				}
			}
		
			array_push($tabDistances, $tabLigneDistances);
			array_push($tabPassages, $tabLignePassages);
		}
		
		return array($tabDistances, $tabPassages);
	}
	
	
	/**
	 * Calcule la distance entre la position du joueur et les objets
	 * @return array
	 */
	public function calculerDistanceObjets() {
		$tabInit = $this->initialiserCalculDistance(array(self::MUR));
		$tabDistances = $tabInit[0];
		$tabPassages = $tabInit[1];
		
		while (($coords = $this->chercherMinimum($tabDistances, $tabPassages)) != array(-1, -1)) {
			$ligne = $coords[0];
			$colonne = $coords[1];
				
			//On indique le fait qu'on est passé par cette case.
			$tabPassages[$ligne][$colonne] = 1;
			$objetCase = $this->cases[$ligne][$colonne];
			$distanceCase = $tabDistances[$ligne][$colonne];
				
			if (($objetCase != self::MINE_NEUTRE) && ($objetCase != self::TAVERNE)) {
				// On consulte l'élément à la gauche du joueur
				if (($ligne > 0) && ($tabPassages[$ligne - 1][$colonne] != 1) && ($tabDistances[$ligne - 1][$colonne] > $distanceCase + 1)) {
					$tabDistances[$ligne - 1][$colonne] = $distanceCase + 1;
				}
		
				// On consulte l'élément à la droite du joueur
				if (($ligne < $this->taille - 1) && ($tabPassages[$ligne + 1][$colonne] != 1) && ($tabDistances[$ligne + 1][$colonne] > $distanceCase + 1)) {
					$tabDistances[$ligne + 1][$colonne] = $distanceCase + 1;
				}
		
				// On consulte l'élément au-dessus du joueur
				if (($colonne > 0) && ($tabPassages[$ligne][$colonne - 1] != 1) && ($tabDistances[$ligne][$colonne - 1] > $distanceCase + 1)) {
					$tabDistances[$ligne][$colonne - 1] = $distanceCase + 1;
				}
		
				// On consulte l'élément en dessous du joueur
				if (($colonne < $this->taille - 1) && ($tabPassages[$ligne][$colonne + 1] != 1) && ($tabDistances[$ligne][$colonne + 1] > $distanceCase + 1)) {
					$tabDistances[$ligne][$colonne + 1] = $distanceCase + 1;
				}
			}
		}
		
		return $tabDistances;
	}
	
	
	/**
	 * Calcule la distance entre la position du joueur et les objets
	 * @return array
	 */
	public function calculerDistanceEnnemis() {
		$tabInit = $this->initialiserCalculDistance(array(self::MUR, self::MINE_NEUTRE));
		$tabDistances = $tabInit[0];
		$tabPassages = $tabInit[1];
	
		while (($coords = $this->chercherMinimum($tabDistances, $tabPassages)) != array(-1, -1)) {
			$ligne = $coords[0];
			$colonne = $coords[1];
	
			//On indique le fait qu'on est passé par cette case.
			$tabPassages[$ligne][$colonne] = 1;
			$objetCase = $this->cases[$ligne][$colonne];
			$distanceCase = $tabDistances[$ligne][$colonne];
	
			if (($objetCase != self::JOUEUR_ENNEMI) && ($objetCase != self::TAVERNE)) {
				// On consulte l'élément à la gauche du joueur
				if (($ligne > 0) && ($tabPassages[$ligne - 1][$colonne] != 1) && ($tabDistances[$ligne - 1][$colonne] > $distanceCase + 1)) {
					$tabDistances[$ligne - 1][$colonne] = $distanceCase + 1;
				}
	
				// On consulte l'élément à la droite du joueur
				if (($ligne < $this->taille - 1) && ($tabPassages[$ligne + 1][$colonne] != 1) && ($tabDistances[$ligne + 1][$colonne] > $distanceCase + 1)) {
					$tabDistances[$ligne + 1][$colonne] = $distanceCase + 1;
				}
	
				// On consulte l'élément au-dessus du joueur
				if (($colonne > 0) && ($tabPassages[$ligne][$colonne - 1] != 1) && ($tabDistances[$ligne][$colonne - 1] > $distanceCase + 1)) {
					$tabDistances[$ligne][$colonne - 1] = $distanceCase + 1;
				}
	
				// On consulte l'élément en dessous du joueur
				if (($colonne < $this->taille - 1) && ($tabPassages[$ligne][$colonne + 1] != 1) && ($tabDistances[$ligne][$colonne + 1] > $distanceCase + 1)) {
					$tabDistances[$ligne][$colonne + 1] = $distanceCase + 1;
				}
			}
		}
	
		return $tabDistances;
	}
	
	
	/**
	 * Recherche le prochain élément à parcourir.
	 * @param array $tabDistances
	 * @param array $tabPassages
	 * @return array
	 */
	private function chercherMinimum($tabDistances, $tabPassages) {
		$coordMin = array(-1, -1);
		$min = PHP_INT_MAX;
		
		for ($ligne = 0; $ligne < $this->taille; $ligne++) {				
			for ($colonne = 0; $colonne < $this->taille; $colonne++) {
				if (($tabPassages[$ligne][$colonne] == 0) && ($tabDistances[$ligne][$colonne] >= 0) && ($tabDistances[$ligne][$colonne] < $min)) {
					$coordMin[0] = $ligne;
					$coordMin[1] = $colonne;
					$min = $tabDistances[$ligne][$colonne];
				}
			}
		}
		
		return $coordMin;
	}
	
	
	/**
	 * Détermine à quelle distance l'objet recherché le plus proche se trouve du joueur.
	 * @return array
	 */
	private function obtenirDistanceObjet($tabDistances, $objet) {
		$coords = array(-1, -1);
		$distance = PHP_INT_MAX;
		
		for ($i=0; $i<$this->taille; $i++) {
			for($j=0; $j<$this->taille; $j++) {
				if (($objet == $this->cases[$i][$j]) && ($distance > $tabDistances[$i][$j])) {
					$coords[0] = $i;
					$coords[1] = $j;
					$distance = $tabDistances[$i][$j];
				}
			}
		}
		
		return $coords;
	}
	
	/**
	 * Donne la position de la taverne la plus proche.
	 * @param array $tabDistances
	 * @return array
	 */
	public function obtenirTaverne($tabDistances) {
		return $this->obtenirDistanceObjet($tabDistances, self::TAVERNE);
	}
	
	
	/**
	 * Donne la position de la mine la plus proche.
	 * @param array $tabDistances
	 * @return array
	 */
	public function obtenirMine($tabDistances) {
		return $this->obtenirDistanceObjet($tabDistances, self::MINE_NEUTRE);
	}
	
	
	/**
	 * Donne la position de l'ennemi le plus proche.
	 * @param array $tabDistances
	 * @return array
	 */
	public function obtenirEnnemiProche($tabDistances) {
		return $this->obtenirDistanceObjet($tabDistances, self::JOUEUR_ENNEMI);
	}
	
	
	/**
	 * Donne la position de l'ennemi le plus riche.
	 * @param array $tabHeros
	 * @return array
	 */
	public function obtenirEnnemi($tabHeros) {
		$nbMines = 0;
		$coords = array(-1, -1);
		
		for ($i = 0; $i < 4; $i++) {
			if ($i != $this->idJoueur - 1) {
				if (($tabHeros[$i]["mineCount"] >= $nbMines)) {
					$nbMines = $tabHeros[$i]["mineCount"];
					$coords[0] = $tabHeros[$i]["pos"]["x"];
					$coords[1] = $tabHeros[$i]["pos"]["y"];
				}
			}
		}
		
		return $coords;
	}
	
	
	/**
	 * Réinitialise les distances pour un objet donné pour éviter de se diriger vers celui-ci.
	 * @param array $tabDistances
	 * @param int $objet
	 * @return array
	 */
	private function reinitialiserDistance($tabDistances, $objet) {
		for ($i=0; $i<$this->taille; $i++) {
			for ($j=0; $j<$this->taille; $j++) {				
				if ($objet == $this->cases[$i][$j]) {
					$tabDistances[$i][$j] = PHP_INT_MAX;
				}
			}
		}
		
		return $tabDistances;
	}
	
	
	/**
	 * Réinitialise les distances pour chaque taverne.
	 * @param array $tabDistances
	 * @return array
	 */
	public function reinitialiserTaverne($tabDistances) {
		return $this->reinitialiserDistance($tabDistances, self::TAVERNE);
	}
	
	
	/**
	 * Réinitialise les distances pour chaque mine.
	 * @param array $tabDistances
	 * @return array
	 */
	public function reinitialiserMine($tabDistances) {
		return $this->reinitialiserDistance($tabDistances, self::MINE_NEUTRE);
	}
	
	
	/**
	 * Réinitialise les distances pour chaque ennemi.
	 * @param array $tabDistances
	 * @return array
	 */
	public function reinitialiserEnnemi($tabDistances) {
		return $this->reinitialiserDistance($tabDistances, self::JOUEUR_ENNEMI);
	}
}