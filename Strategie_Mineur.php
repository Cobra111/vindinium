<?php
require_once "Strategie.php";


final class Strategie_Mineur implements Strategie {

	public static function move($state, $idPlayer) {
		//Initialisation
		$coordsJoueur = array($state["hero"]["pos"]["x"], $state["hero"]["pos"]["y"]);
		
		$map = new Map($state['game']['board'], $idPlayer + 1);
		$tableauDistance = $map->calculerDistanceObjets();
		$coordsTav = $map->obtenirTaverne($tableauDistance);
		
		$heroState = $state["game"]["heroes"][$idPlayer];
		$taille = $state["game"]["board"]["size"];
		
		
		//Calcul de la direction à prendre
		if (($heroState["life"] < 30) || (($tableauDistance[$coordsTav[0]][$coordsTav[1]] == 1) && ($heroState['gold'] > 0) && ($heroState["life"] <= 90))
				|| ($state["hero"]["mineCount"] == $map->getNbMines() && ($heroState["life"] <= 90)) ) {
			//Boire un coup
			$coords = $coordsTav;
			$tableauDistance = $map->reinitialiserMine($tableauDistance);
		}
		else {
			//Aller à la mine
			$coords = $map->obtenirMine($tableauDistance);
			$tableauDistance = $map->reinitialiserTaverne($tableauDistance);
		}
		
		
		$direction = 'Stay';
		
		//On fait le chemin inverse
		if ($coords != array(-1, -1)) {
			while ($coords != $coordsJoueur) {
				$ligne = $coords[0];
				$colonne = $coords[1];
				
				//Aller à l'ouest
				if ($coords[1] < $taille - 1 && $tableauDistance[$ligne][$colonne + 1] == $tableauDistance[$ligne][$colonne] - 1)
				{
					$coords[1]++;
					$direction = 'West';
				}
				
				//Aller à l'est
				else if ($coords[1] > 0 && $tableauDistance[$ligne][$coords[1] - 1] == $tableauDistance[$ligne][$coords[1]] - 1)
				{
					$coords[1]--;
					$direction = 'East';
				}
				
				//Aller au nord
				else if ($coords[0] < $taille - 1 && $tableauDistance[$ligne + 1][$colonne] == $tableauDistance[$ligne][$colonne] - 1)
				{
					$coords[0]++;
					$direction = 'North';
				}
				
				//Aller au sud
				else if ($coords[0] > 0 && $tableauDistance[$ligne - 1][$colonne] == $tableauDistance[$ligne][$colonne] - 1)
				{
					$coords[0]--;
					$direction = 'South';
				}
			}
		}
		
		return $direction;
	}
}